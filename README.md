# WSL

- [ ] Figure out how to use LxRunOffline to use the same user / terminal / icon as when using stock Ubuntu
- [ ] Try [this](https://github.com/therealkenc/EnumWSL) for obtaining the icon path

[Manage and configure WSL](https://docs.microsoft.com/en-us/windows/wsl/wsl-config)

- `Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux`
- Enable Developer Mode
- `bash`
- `wslconfig`
- `wsl` runs the default distribution (also `bash`)
- Can also run by name:
    - `ubuntu`
    - `opensuse-42`
    - …
- `HKCU\Software\Microsoft\Windows\CurrentVersion\lxss`

[LxRunOffline](https://github.com/DDoSolitary/LxRunOffline)


(`xenial.tar.gz` is previously downloaded from [Canonical](https://partner-images.canonical.com/core/xenial/current/ubuntu-xenial-core-cloudimg-amd64-root.tar.gz).)

Create using `xenial.ps1`:

- [ ] Use `param` to accept the name through command line
- [ ] Make icon in PowerShell

```powershell
cd C:/Users/Tom/Desktop/wsl/
$NAME="Demo"
lxrunoffline install -n $NAME -f xenial.tar.gz -d $NAME
lxrunoffline run -n $NAME
chmod +x xenial.sh
./xenial.sh
```

Setup using `xenial.sh`:

```bash
#!/usr/bin/env bash
echo "Setting up Xenial…"
apt install -y lsb_release nano
echo "export DISPLAY=:0" >> ~/.bashrc
```

Link:

- Explorer > New Shortcut
- Path: `lxrunoffline run -n $NAME -w`
- Name: `$NAME`
- Icon:

## Next Steps

- [Setting Up Docker for Windows and WSL to Work Flawlessly](https://nickjanetakis.com/blog/setting-up-docker-for-windows-and-wsl-to-work-flawlessly)
- [WSL tutorial](https://github.com/QMonkey/wsl-tutorial)
- [LoWe](https://github.com/kpocza/LoWe)
- [Hands-On with WSL: Running Graphical Apps](https://virtualizationreview.com/articles/2018/01/30/hands-on-with-wsl-graphical-apps.aspx)
- [Background task support for WSL](https://blogs.msdn.microsoft.com/commandline/2017/12/04/background-task-support-in-wsl/)

## Sources

- [Getting Crazy with WSL](https://brianketelsen.com/getting-crazy-with-windows-subsystem-for-linux/)
    - LxRunOffline introduction

## Links

- [WSL for Nerds](https://blog.jessfraz.com/post/windows-for-linux-nerds/)
- [User Voice for WSL](https://wpdev.uservoice.com/forums/266908-command-prompt-console-windows-subsystem-for-l/filters/top)
